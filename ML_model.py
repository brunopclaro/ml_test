# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'

#UPDATE TEST with SSH key confirm

from IPython import get_ipython
import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt
import seaborn as sns



# %%
#Column names

cols=['MPG','Cylinders','Displacement','Horsepower','Weight','Acceleration','Model_Year','Origin']

# reading the .data file using pandas

df=pd.read_csv('/home/brunopclaro/flask_ml/auto-mpg.data',names=cols,na_values='?',comment = '\t',sep=' ',skipinitialspace=True)

#making copy of dataframe

data=df.copy()


data=data.drop('Origin',axis=1)

data.head()




# %%
from pandas_profiling import ProfileReport

ProfileReport(data, minimal=True)


# %%
# Setting aside test data - using stratified sampling

from sklearn.model_selection import StratifiedShuffleSplit

split = StratifiedShuffleSplit(n_splits=10, test_size=0.2, random_state=0)

for train_index, test_index in split.split(data,data["Cylinders"]):
    strat_train_set=data.loc[train_index]
    strat_test_set=data.loc[test_index]




# %%
#checking for cylinder category distribution in training set

strat_train_set['Cylinders'].value_counts() / len(strat_train_set)


# %%
#data cleaning - Imputer
##handling missing values
from sklearn.impute import SimpleImputer

imputer= SimpleImputer(strategy="median")
imputer.fit(data)


# %%
# attribute addition  - addint custom transformation
from sklearn.base import BaseEstimator, TransformerMixin

acc_ix, hpower_ix, cyl_ix = 4, 2, 0

##curtom class inheriting the BaseEstimator and TransformerMixin


class CustomAttrAdder(BaseEstimator,TransformerMixin):
    
    def __init__(self,acc_on_power=True):
        self.acc_on_power = acc_on_power  #new optimal variable
    def fit(self, X,y=None):
        return self #nothing else to do
    def transform (self, X):
        acc_on_cyl = X[:, acc_ix] / X[:,cyl_ix] #required new variable
        if self.acc_on_power:
            acc_on_power = X[:, acc_ix] / X[:, hpower_ix]
            return np.c_[X, acc_on_power, acc_on_cyl] # returns 2D array

        return np.c_[X, acc_on_cyl]


attr_adder=CustomAttrAdder(acc_on_power=True)
data_extra_attrs = attr_adder.transform(data.values)
data_extra_attrs[0]




# %%
# Setting up data transformation pipeline for numerical attributes
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.compose import ColumnTransformer

def num_pipeline_transformer(data):
    '''
    Function to process numerical transformations
    Argument:
        data: original dataframe
    Returns:
        num_attrs: numerical dataframe
        num_pipeline: numerial pipeline object
    '''

    numerics = ['float64','int64']

    num_attrs = data.select_dtypes(include=numerics)

    num_pipeline = Pipeline([
        ('imputer', SimpleImputer(strategy="median")),
        ('attr_adder', CustomAttrAdder()),
        ('std_scaler', StandardScaler()),
        ])

    return num_attrs, num_pipeline


# %%
def pipeline_transformer(data):

    num_attrs, num_pipeline = num_pipeline_transformer(data)
    full_pipeline= ColumnTransformer([
        ("num",num_pipeline,list(num_attrs))
    ])

    prepared_data=full_pipeline.fit_transform(data)
    return prepared_data
    


# %%
data_features=data.iloc[:,1:7]
prepared_data=pipeline_transformer(data_features)
prepared_data




# %%
data_labels=data.iloc[:,0]
data_labels


# %%
#Selecting and training ML models


from sklearn.linear_model import LinearRegression
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import RandomForestRegressor
from sklearn.tree import DecisionTreeRegressor


lin_reg = RandomForestRegressor()
lin_reg.fit(prepared_data,data_labels)




# %%
#testing predictions

sample_data = data_features.iloc[:5,]
sample_labels=data_labels.iloc[:5]

sample_data_prepared = pipeline_transformer(sample_data)

print("prediction of samples:",lin_reg.predict(sample_data_prepared))


# %%
print("Actual labels of samples",list(sample_labels))


# %%
#Evaluating model
from sklearn.metrics import mean_squared_error

mpg_predictions = lin_reg.predict(prepared_data)
lin_mse =  mean_squared_error(data_labels, mpg_predictions)
lin_rmse = np.sqrt(lin_mse)
lin_rmse


# %%
# cross-validation

from sklearn.model_selection import cross_val_score

scores = cross_val_score(lin_reg,prepared_data,data_labels,scoring="neg_mean_squared_error",cv=10)

reg_rmse_scores = np.sqrt(-scores)


# %%
reg_rmse_scores.mean()


# %%
#Fine-tuning hyper parameters

from sklearn.model_selection import GridSearchCV

param_grid =[
    {'n_estimators': [3, 10, 30], 'max_features': [2,4,6,8]},
    {'bootstrap': [False], 'n_estimators': [3,10], 'max_features': [2,3,4]},
]

forest_reg=RandomForestRegressor()

grid_search = GridSearchCV(forest_reg, param_grid,scoring="neg_mean_squared_error",return_train_score=True,cv=10)

grid_search.fit(prepared_data,data_labels)


# %%
grid_search.best_params_


# %%
cv_scores=grid_search.cv_results_

##printing all parameters along with their scores

for mean_score, params in zip(cv_scores['mean_test_score'], cv_scores["params"]):
    print(np.sqrt(-mean_score),params)


# %%
# feature importances

feature_importances = grid_search.best_estimator_.feature_importances_

extra_features= ["acc_on_power","acc_on_cyl"]
numerics= ['float64','int64']
num_features= list(data.select_dtypes(include=numerics))

features=num_features+extra_features
sorted(zip(features, feature_importances),reverse=True)


# %%
# Evaluate Entire System

#capturing best configuration

final_model=grid_search.best_estimator_





# %%
##segregating target variable from test set

X_test= strat_test_set.drop("MPG",axis=1)
Y_test= strat_test_set["MPG"].copy()


# %%
#preparing data with final transformation

X_test_prepared=pipeline_transformer(X_test)


# %%
#making final predictions

final_predictions= final_model.predict(X_test_prepared)
final_mse = mean_squared_error(Y_test, final_predictions)
final_rmse = np.sqrt(final_mse)


# %%
final_model


# %%
final_rmse


# %%
#Function to cover flow


def predict_mpg(config,model):

    if type (config) == dict:
        df=pd.DataFrame(config)
    else:
        df=config
    
    
    vc=pipeline_transformer(df)
    print(len(vc[0]))
    vc_pred=model.predict(vc)
    return vc_pred


# %%
# testing model uploading - vehicle config

vehicle_config = {'Cylinders': [5, 6, 8],'Displacement': [193, 160.0, 165.5],'Horsepower': [104, 130.0, 98.0],'Weight': [2970, 3150.0, 2600.0], 'Acceleration': [15, 14.0, 16.0],'Model_Year': [76, 80, 78]}

predict_mpg(vehicle_config,final_model)


# %%
# dump final model info file

import pickle
import json
import _pickle as cPickle

import joblib

with open('/home/brunopclaro/flask_ml/model.pkl','wb') as f_out:
    pickle.dump(final_model, f_out)  #writing final model in .bin file
    f_out.close()  #close the file


# %%
##loading model from file

with open('/home/brunopclaro/flask_ml/model.pkl','rb') as f_in:
    model=pickle.load(f_in)

pred_config=predict_mpg(vehicle_config,model)
print(pred_config)
