
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import OneHotEncoder
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.impute import SimpleImputer

from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.compose import ColumnTransformer

##functions

# attribute addition  - addint custom transformation


acc_ix, hpower_ix, cyl_ix = 4, 2, 0

##curtom class inheriting the BaseEstimator and TransformerMixin


class CustomAttrAdder(BaseEstimator,TransformerMixin):
    
    def __init__(self,acc_on_power=True):
        self.acc_on_power = acc_on_power  #new optimal variable
    def fit(self, X,y=None):
        return self #nothing else to do
    def transform (self, X):
        acc_on_cyl = X[:, acc_ix] / X[:,cyl_ix] #required new variable
        if self.acc_on_power:
            acc_on_power = X[:, acc_ix] / X[:, hpower_ix]
            return np.c_[X, acc_on_power, acc_on_cyl] # returns 2D array

        return np.c_[X, acc_on_cyl]




def predict_mpg(config,model):
    
    if type(config) == dict:
        data = pd.DataFrame(config)
    else:
        data = config

    vc = pipeline_transformer(data)
    print(len(vc[0]))
    vc_pred = model.predict(vc)
    return vc_pred




def num_pipeline_transformer(data):

    numerics = ['float64', 'int64']

    num_attrs = data.select_dtypes(include=numerics)

    num_pipeline = Pipeline([
    ('imputer', SimpleImputer(strategy="median")),
    ('attr_adder', CustomAttrAdder()),
    ('std_scaler', StandardScaler())
    ])

    return num_attrs, num_pipeline

def pipeline_transformer(data):
        
    num_attrs, num_pipeline = num_pipeline_transformer(data)
    full_pipeline = ColumnTransformer([
        ("num", num_pipeline, list(num_attrs))
    ])

    prepared_data = full_pipeline.fit_transform(data)
    return prepared_data


