from flask import Flask,render_template,jsonify,request,redirect,url_for
from flask_sqlalchemy import SQLAlchemy
import pickle
from ml import predict_mpg, num_pipeline_transformer, pipeline_transformer
import pandas as pd
from formencode import variabledecode
import _pickle as cPickle
import joblib
import sqlite3
from flask_mysqldb import MySQL


application = Flask(__name__)

# config MySQL

application.config['MYSQL_HOST'] = 'aa8yn1qhc75ny3.cqhkfdicggdc.eu-west-1.rds.amazonaws.com'
application.config['MYSQL_USER'] = 'brunopclaro'
application.config['MYSQL_PASSWORD'] = 'Dj@ngoSmut'
application.config['MYSQL_DB'] = 'Cars'

#init MYSQL
db = SQLAlchemy(application)





class User(db.Model):
    __tablename__ = "CarsDetails"

    id = db.Column(db.Integer, primary_key= True, autoincrement= True)
    Cylinders = db.Column(db.Integer, default=0)
    Displacement = db.Column(db.Integer, default=0)
    Horsepower = db.Column(db.Integer, default=0)
    Weight = db.Column(db.Integer, default=0)
    Acceleration = db.Column(db.Integer, default=0)
    Model_Year = db.Column(db.Integer, default=0)
    MPG = db.Column(db.Integer, default=0)

    def __init__(self, Cylinders, Displacement, Horsepower, Weight, Acceleration, Model_Year, MPG):
        self.Cylinders = Cylinders
        self.Displacement = Displacement
        self.Horsepower = Horsepower
        self.Weight = Weight
        self.Acceleration = Acceleration
        self.Model_Year = Model_Year
        self.MPG = MPG

@application.route("/")
def home():
    return redirect(url_for('index'))

@application.route("/index", methods = ["GET", "POST"])
def index():
    if request.method == 'POST':
        data = request.form.to_dict(flat=False)
        Cylinders = data["Cylinders"]
        Cylinders = float(Cylinders[0])
        Displacement = data["Displacement"]
        Displacement = float(Displacement[0])
        Horsepower = data["Horsepower"]
        Horsepower = float(Horsepower[0])
        Weight = data["Weight"]
        Weight = float(Weight[0])
        Acceleration = data["Acceleration"]
        Acceleration = float(Acceleration[0])
        Model_Year = data["Model_Year"]
        Model_Year = float(Model_Year[0])

        data_int = dict((k, [float(s) for s in v]) for k,v in data.items())

        con = db.connect()

        data_all = pd.read_sql_query("SELECT Cylinders,Displacement,Horsepower,Weight,Acceleration,Model_Year from CarsDetails",con)
        
        data_in = pd.DataFrame(data_int)

        data_all = pd.DataFrame.append(data_all,data_in)

        with open('/home/brunopclaro/ml_test/model.pkl', 'rb') as f_in:
            model = joblib.load(f_in)

        
        MPG = predict_mpg(data_all,model)
        MPG = MPG[-1]
        

        new_data = User(Cylinders, Displacement, Horsepower, Weight, Acceleration, Model_Year,MPG)
        
        db.session.add(new_data)
        db.session.commit()
        user_data = User.query.all()

        



        return render_template("index.html", user_data = user_data)

    return render_template("index.html")

if __name__=="__main__":
    db.create_all()
    application.debug=True
    application.run(host='0.0.0.0',port=8080)

